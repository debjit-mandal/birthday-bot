# WhatsApp Birthday Bot
A WhatsApp birthday bot using Python and the Twilio API.The bot will send birthday greetings to your contacts on their special day. Make sure you have a Twilio account and the necessary credentials before you begin.

Remember to replace the placeholders `your_account_sid`, `your_auth_token`, and `your_twilio_phone_number` with your actual Twilio credentials.

Before running this code, make sure that the twilio library is installed. To install it using pip:

`pip install twilio`

To run this code locally:

`sudo git clone https://github.com/debjit-mandal/birthday-bot`

`cd birhday-bot`

`python main.py`

Please feel free to suggest any kind of improvement.
