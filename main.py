from datetime import datetime
from twilio.rest import Client
import json

# Twilio credentials
account_sid = 'your_account_sid'
auth_token = 'your_auth_token'
twilio_phone_number = 'your_twilio_phone_number'

# Function to add a new contact and their birthday
def add_contact():
    name = input("Enter the contact's name: ")
    dob = input("Enter the contact's birthday (YYYY-MM-DD): ")

    contacts[name] = dob
    print(f"Contact {name} added successfully!")

# Function to remove a contact
def remove_contact():
    name = input("Enter the contact's name you want to remove: ")

    if name in contacts:
        del contacts[name]
        print(f"Contact {name} removed successfully!")
    else:
        print(f"Contact {name} not found!")

# Function to view the list of contacts
def view_contacts():
    print("\nContact List:")
    if contacts:
        for name, dob in contacts.items():
            print(f"{name}: {dob}")
    else:
        print("No contacts found.")

# Function to edit contact details
def edit_contact():
    name = input("Enter the contact's name you want to edit: ")

    if name in contacts:
        print(f"Current birthday for {name}: {contacts[name]}")
        new_dob = input("Enter the new birthday (YYYY-MM-DD): ")
        contacts[name] = new_dob
        print(f"Contact {name} updated successfully!")
    else:
        print(f"Contact {name} not found!")

# Function to search for a contact by name
def search_contact():
    name = input("Enter the contact's name you want to search: ")

    if name in contacts:
        print(f"Contact found - {name}: {contacts[name]}")
    else:
        print(f"Contact {name} not found.")

# Function to save contacts to a file
def save_contacts():
    try:
        with open("contacts.json", "w") as file:
            json.dump(contacts, file)
        print("Contacts saved successfully!")
    except Exception as e:
        print(f"Failed to save contacts: {str(e)}")

# Function to load contacts from a file
def load_contacts():
    try:
        with open("contacts.json", "r") as file:
            contacts.update(json.load(file))
        print("Contacts loaded successfully!")
    except FileNotFoundError:
        print("No contacts file found.")
    except Exception as e:
        print(f"Failed to load contacts: {str(e)}")

# Function to send birthday greetings via WhatsApp
def send_birthday_greetings(contact):
    client = Client(account_sid, auth_token)
    message = f"🎉 Happy Birthday, {contact}! 🎂🎈"
    recipient_number = contacts[contact]

    try:
        client.messages.create(
            from_='whatsapp:' + twilio_phone_number,
            body=message,
            to='whatsapp:' + recipient_number
        )
        print(f"Sent birthday message to {contact}")
    except Exception as e:
        print(f"Failed to send birthday message to {contact}: {str(e)}")

# Check if today is someone's birthday
today = datetime.now().strftime('%Y-%m-%d')
birthday_contacts = [name for name, dob in contacts.items() if dob == today]

# Send birthday greetings via WhatsApp
if birthday_contacts:
    for contact in birthday_contacts:
        send_birthday_greetings(contact)
else:
    print("No birthdays today.")

# Prompt the user to add, remove, view, edit, search, save, load, or quit
while True:
    action = input("Do you want to add, remove, view, edit, search, save, load contacts, or quit? ").lower()

    if action == 'add':
        add_contact()
    elif action == 'remove':
        remove_contact()
    elif action == 'view':
        view_contacts()
    elif action == 'edit':
        edit_contact()
    elif action == 'search':
        search_contact()
    elif action == 'save':
        save_contacts()
    elif action == 'load':
        load_contacts()
    elif action == 'quit':
        print("Exiting...")
        break
    else:
        print("Invalid action. Please try again.")

# Display the updated contact list
print("\nUpdated Contact List:")
view_contacts()
